from django.template.context import RequestContext
from django.template.loader import get_template


class Block(object):
    template_name = None

    def __init__(self, request):
        self.request = request

    def get_context_data(self, **kwargs):
        return kwargs

    def render(self, context_data=None, context_object=None):
        template = get_template(self.template_name)
        if context_object is None:
            context_object = RequestContext(self.request)
        if context_data is None:
            context_data = {}
        context_object.update(self.get_context_data(**context_data))
        return template.render(context_object)
