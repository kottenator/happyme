from collections import OrderedDict
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from products.models import Product, Category


class ProductListPage(ListView):
    template_name = 'products/pages/product_list_page.html'
    context_object_name = 'collections'

    def get_queryset(self):
        queryset = Product.objects.available().select_related('images')
        selected_category_id = self._get_int_param('category')

        if selected_category_id is None:
            queryset = self._group_by_overview(queryset)
        else:
            selected_categories = Category.objects.get(id=selected_category_id).get_descendants(include_self=True)
            queryset = queryset.filter(category__in=selected_categories)
            queryset = self._group_by_collection(queryset)

        return queryset

    def _get_int_param(self, name):
        try:
            param = int(self.request.GET[name])
        except (ValueError, MultiValueDictKeyError):
            param = None
        return param

    def _group_by_collection(self, queryset):
        queryset = queryset.extra(select={
            'collection_is_null': 'collection_id IS NULL',
        })

        queryset = queryset.select_related('collection').order_by(
            'collection_is_null', 'collection__order_number', 'order_number'
        )

        grouped_queryset = OrderedDict()

        for product in queryset:
            items = grouped_queryset.setdefault((product.category, product.collection), [])
            items.append(product)

        return grouped_queryset

    def _group_by_overview(self, queryset):
        queryset = queryset.extra(select={
            'collection_is_null': 'collection_id IS NULL',
        })

        queryset = queryset.select_related('category', 'collection').order_by(
            'category__order_number', 'collection_is_null', 'collection__order_number', 'order_number'
        )

        grouped_queryset = OrderedDict()
        categories_overview = {}

        for product in queryset:
            category = product.category
            collection = product.collection

            if category not in categories_overview:
                categories_overview[category] = collection

            stored_collection = categories_overview[category]

            if stored_collection == collection:
                items = grouped_queryset.setdefault((category, collection), [])
                items.append(product)

        return grouped_queryset

    def get_context_data(self, **kwargs):
        data = super(ProductListPage, self).get_context_data(**kwargs)
        data['items_limit'] = 4
        return data

product_list_page = ProductListPage.as_view()


class ProductDetailPage(DetailView):
    template_name = 'products/pages/product_detail_page.html'
    model = Product
    context_object_name = 'product'

    def get_context_data(self, **kwargs):
        data = super(ProductDetailPage, self).get_context_data(**kwargs)
        product = self.get_object()
        data['categories'] = product.category.get_ancestors(include_self=True)
        data['collection_data'] = (
            (product.category, product.collection),
            product.collection.available_products() if product.collection
            else product.category.available_products_without_collection()
        )
        data['selected_category_id'] = product.category.get_root().id

        return data

product_detail_page = ProductDetailPage.as_view()
