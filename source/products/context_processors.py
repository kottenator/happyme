from django.conf import settings
from django.utils.datastructures import MultiValueDictKeyError
from products.models import Category


def categories_menu(request):
    res = dict(top_categories=Category.objects.available_top_categories())
    try:
        res['selected_category_id'] = int(request.GET['category'])
    except (ValueError, MultiValueDictKeyError):
        pass
    return res


def site_url(request):
    url = settings.SITE_URL
    if url[-1] == '/':
        url = url[:-1]
    return {
        'SITE_URL': url
    }
