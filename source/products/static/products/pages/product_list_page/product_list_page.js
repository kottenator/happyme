happyme.productListPage = {
    init: function() {
        this._initProductCollections();
    },

    _initProductCollections: function() {
        $('.product-collection-block').each(function() {
            var el = $(this),
                block = new happyme.ProductCollectionBlock(el);
            el.data('product-collection', block);
        });
    }
};

$(function() {
    happyme.productListPage.init();
});
