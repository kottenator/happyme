happyme.productDetailPage = {
    /**
     * Photo gallery block
     * @type {happyme.ProductGalleryBlock}
     */
    gallery: null,

    init: function() {
        this._initGallery();
    },

    /** @private */
    _initGallery: function() {
        this.gallery = new happyme.ProductGalleryBlock({ container: $('.product-gallery-block') });
    }
};

$(function() {
    happyme.productDetailPage.init();
});
