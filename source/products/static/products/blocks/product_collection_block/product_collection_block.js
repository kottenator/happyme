happyme.ProductCollectionBlock = function(container) {
    container && this.init(container);
};

happyme.ProductCollectionBlock.prototype = {
    /** @type {jQuery} */
    container: null,

    init: function(container) {
        this.container = container;
        this._initMoreButton();
    },

    _initMoreButton: function() {
        var container = this.container;

        container.find('.more').click(function(e) {
            e.preventDefault();
            $(this).hide();
            container.find('.product-block.hidden').removeClass('hidden');
        })
    }
};
