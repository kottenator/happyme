happyme.ProductGalleryBlock = function(cfg) {
    cfg && this.init(cfg);
};

happyme.ProductGalleryBlock.prototype = {
    /**
     * Container element
     * @type {jQuery}
     */
    container: null,

    /**
     * Media thumbnails
     * @type {jQuery}
     */
    thumbnails: null,

    /**
     * Media preview block
     * @type {jQuery}
     */
    preview: null,

    /**
     * Dictionary that contains width and height of the preview area
     * Calculated once on page load
     * @private
     * @type {object}
     */
    _previewCache: null,

    /**
     * Zoom navigator rectangle on preview block
     * @type {jQuery}
     */
    previewNavigator: null,

    /**
     * Dictionary that contains width and height of the zoom preview navigator rectangle (in the preview area)
     * Calculated each time user selects a thumbnail
     * @private
     * @type {object}
     */
    _previewNavigatorCache: null,

    /**
     * Image inside the preview block
     * @type {jQuery}
     */
    previewImage: null,

    /**
     * Dictionary that contains original and scaled width and height of current gallery image
     * Calculated each time user selects a thumbnail
     * @private
     * @type {object}
     */
    _previewImageCache: null,

    /**
     * Large preview block (appears on hover)
     * @type {jQuery}
     */
    zoomPreview: null,

    /**
     * Dictionary that contains width and height of the zoom preview area
     * Calculated once on page load
     * @private
     * @type {object}
     */
    _zoomPreviewCache: null,

    init: function(cfg) {
        $.extend(this, cfg);
        this.thumbnails = this.container.find('.nav').find('li');
        this.preview = this.container.find('.preview');
        this.previewNavigator = this.preview.find('.zoom-navigator');
        this.previewImage = this.preview.find('img');
        this.zoomPreview = this.container.find('.large-preview');

        this._initThumbnails();
        this._initZoom();
    },

    _initThumbnails: function() {
        var self = this;

        this.thumbnails.find('a').click(function(e) {
            e.preventDefault();

            if (self.previewImage.prop('src') == this.href)
                return;

            self._updatePreview(this.href);
            $(this).parent().addClass('selected').siblings().removeClass('selected');
        });
    },

    _updatePreview: function(src) {
        var newPreviewImage = $('<img>', { src: src });
        this.previewImage.replaceWith(newPreviewImage);
        this.previewImage = newPreviewImage;
        this._cachePreviewImage();
        this.zoomPreview.css('background-image', 'url("' + src + '")');
    },

    /**
     * We cache image sizes on image change (to optimize calculations on zoom)
     * @private
     */
    _cachePreviewImage: function() {
        this._previewImageCache = null;

        this.preview.imagesLoaded($.proxy(function(imagesLoadedInstance) {
            // The image loaded too late, another one is in progress or already loaded
            if (imagesLoadedInstance.images[0].img.src != this.previewImage.prop('src'))
                return;

            var previewImage = this.previewImage,
                previewImageWidth = previewImage.width(),
                previewImageHeight = previewImage.height();

            previewImage.css({ 'max-width': 'none', 'max-height': 'none' });

            var imgWidth = previewImage.width(),
                imgHeight = previewImage.height();

            previewImage.css({ 'max-width': '', 'max-height': '' });

            this._previewImageCache = {
                previewWidth: previewImageWidth,
                previewHeight: previewImageHeight,
                originalWidth: imgWidth,
                originalHeight: imgHeight,
                scaleX: imgWidth / previewImageWidth,
                scaleY: imgHeight / previewImageHeight
            };

            this._updatePreviewNavigator();
        }, this));
    },

    _updatePreviewNavigator: function() {
        var navigatorWidth = parseInt(this._zoomPreviewCache.width / this._previewImageCache.scaleX),
            navigatorHeight = parseInt(this._zoomPreviewCache.height / this._previewImageCache.scaleY),
            zoomShortWidth = this._previewImageCache.originalWidth < this._zoomPreviewCache.width,
            zoomShortHeight = this._previewImageCache.originalHeight < this._zoomPreviewCache.height;

        if (zoomShortWidth)
            navigatorWidth = this._previewCache.width;

        if (zoomShortHeight)
            navigatorHeight = this._previewCache.height;

        var nav = this.previewNavigator;
        nav.width(navigatorWidth);
        nav.height(navigatorHeight);

        this._previewNavigatorCache = {
            width: navigatorWidth,
            height: navigatorHeight,
            dx: navigatorWidth / 2,
            dy: navigatorHeight / 2
        }
    },

    _initZoom: function() {
        var self = this;

        this._cachePreviewAreas();
        this._cachePreviewImage();
        this.zoomPreview.css('background-image', 'url("' + this.previewImage.prop('src') + '")');

        this.preview.on('mousemove', function(e) {
            if (self._zoomNeeded()) {
                if (!self.container.hasClass('zooming'))
                    self.container.addClass('zooming');
                self._zoom(e);
            }
        });

        this.preview.on('mouseleave', function() {
            self.container.removeClass('zooming');
        });
    },

    _cachePreviewAreas: function() {
        var previewOffset = this.preview.offset();

        this._previewCache = {
            width: this.preview.width(),
            height: this.preview.height(),
            x: previewOffset.left,
            y: previewOffset.top
        };

        this._zoomPreviewCache = {
            width: this.zoomPreview.width(),
            height: this.zoomPreview.height()
        };
    },

    _zoomNeeded: function() {
        var cache = this._previewImageCache;
        return !!cache && (cache.scaleX > 1 || cache.scaleY > 1);
    },

    _zoom: function(event) {
        var preview = this._previewCache,
            previewImage = this._previewImageCache,
            zoomPreview = this._zoomPreviewCache;

        // Calculate navigator position
        var x = event.pageX - preview.x,
            y = event.pageY - preview.y;

        x = Math.max(0, x - this._previewNavigatorCache.dx);
        y = Math.max(0, y - this._previewNavigatorCache.dy);
        x = Math.min(x, preview.width - this._previewNavigatorCache.width);
        y = Math.min(y, preview.height - this._previewNavigatorCache.height);

        this.previewNavigator.css({ left: x, top: y});

        // Calculate background position for the zoom preview
        var bgPosX = parseInt(((preview.width - previewImage.previewWidth) / 2 - x) * previewImage.scaleX) + 'px',
            bgPosY = parseInt(((preview.height - previewImage.previewHeight) / 2 - y) * previewImage.scaleY) + 'px';

        // zoom "short" width case
        if (previewImage.originalWidth < zoomPreview.width)
            bgPosX = '50%';

        // zoom "short" height case
        if (previewImage.originalHeight < zoomPreview.height)
            bgPosY = '50%';

        this.zoomPreview.css('background-position', bgPosX + ' ' + bgPosY);
    }
};
