from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager


class BaseModel(models.Model):
    title = models.CharField(max_length=100)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class OrderedModel(BaseModel):
    order_number = models.IntegerField(default=0)

    class Meta:
        abstract = True
        ordering = ('order_number',)

    def __str__(self):
        return self.title


class ProductManager(models.Manager):
    def available(self):
        return self.get_queryset().filter(available_amount__gt=0)


class Product(OrderedModel):
    id = models.CharField(verbose_name='Article', max_length=20, primary_key=True)
    description = models.TextField(blank=True)
    category = TreeForeignKey('Category', related_name='products')
    collection = models.ForeignKey('Collection', blank=True, null=True, related_name='products')
    available_amount = models.IntegerField(default=0)
    
    official_price_in_usd = models.FloatField(default=0.0)
    purchase_price_in_usd = models.FloatField(default=0.0)
    price_in_usd = models.FloatField(default=0.0)
    price_in_uah = models.FloatField(blank=True, null=True, default=0.0)
    sale_price_in_uah = models.FloatField(blank=True, null=True)

    # filters
    manufacturer = models.ForeignKey('Manufacturer', blank=True, null=True)
    material = models.ForeignKey('Material', blank=True, null=True)
    color = models.ForeignKey('Color', blank=True, null=True)
    size = models.ForeignKey('Size', blank=True, null=True)

    objects = ProductManager()

    def get_absolute_url(self):
        return reverse('product-detail', args=(self.pk,))

    def get_main_image(self):
        try:
            return self.images.all()[0]
        except IndexError:
            return None


class CategoryManager(TreeManager):
    def available_top_categories(self):
        top_categories = self.root_nodes()

        # filter root nodes to those who has some available products inside
        for category in list(top_categories):
            available = category.get_descendants(include_self=True).filter(products__available_amount__gt=0).count()
            if not available:
                top_categories = top_categories.exclude(id=category.id)

        return top_categories


class Category(MPTTModel, OrderedModel):
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    objects = CategoryManager()

    class MPTTMeta:
        order_insertion_by = ('order_number',)

    def available_products_without_collection(self):
        return self.products.filter(collection__isnull=True, available_amount__gt=0)


class Collection(OrderedModel):
    description = models.TextField(blank=True)
    manufacturer = models.ForeignKey('Manufacturer', blank=True, null=True)
    author = models.ForeignKey('Author', blank=True, null=True)

    def available_products(self):
        return self.products.all().filter(available_amount__gt=0)


class Manufacturer(OrderedModel):
    country = models.CharField(max_length=100)


class Author(BaseModel):
    pass


class Material(OrderedModel):
    pass


class Color(OrderedModel):
    code = models.CharField(max_length=100, blank=True, null=True)
    image = models.ImageField(upload_to='colors', blank=True, null=True)


class Size(OrderedModel):
    pass


def upload_product_image_to(product_image, image_name):
    """
    Helper function for Django 1.7 which cannot serialize lambda functions anymore

    :param products.ProductImage product_image:
    :param string image_name:
    :return:
    """
    return 'products/%s/%s' % (product_image.product.id, image_name)


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images')
    image = models.ImageField(upload_to=upload_product_image_to)
    description = models.TextField(blank=True)


@receiver(post_save, sender=Category)
def rebuild_categories(*args, **kwargs):
    Category.objects.rebuild()
