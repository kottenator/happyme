from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    'products.views',
    url(r'^$', 'product_list_page', name='product-list'),
    url(r'^product/(?P<pk>.+)/$', 'product_detail_page', name='product-detail'),
)
