from django.contrib import admin
import mptt.admin
from sorl.thumbnail.shortcuts import get_thumbnail
from products.models import Product, Category, ProductImage, Collection, Manufacturer, Material, Color, Size, Author


class OrderedModelAdmin(admin.ModelAdmin):
    list_display = ('title', 'order_number')
    list_editable = ('order_number',)


class ProductImagesInline(admin.TabularInline):
    model = ProductImage


class ProductAdmin(OrderedModelAdmin):
    list_display = ('title', 'id', 'get_image_preview', 'category', 'available_amount', 'order_number')
    list_filter = ('category', 'collection')
    inlines = [ProductImagesInline]

    def get_image_preview(self, obj):
        try:
            image = obj.get_main_image().image
            thumbnail = get_thumbnail(image, '100x100', crop='center')
            return '<img width="100" src="%s" alt="image">' % thumbnail.url
        except:
            return '--'
    get_image_preview.short_description = 'Image'
    get_image_preview.allow_tags = True

    def get_readonly_fields(self, request, obj=None):
        return ['id'] if obj else []

admin.site.register(Product, ProductAdmin)


class CategoryAdmin(mptt.admin.MPTTModelAdmin, OrderedModelAdmin):
    mptt_level_indent = 20  # specify pixel amount for this ModelAdmin only

admin.site.register(Category, CategoryAdmin)

admin.site.register(Collection, OrderedModelAdmin)
admin.site.register(Manufacturer, OrderedModelAdmin)
admin.site.register(Author, admin.ModelAdmin)
admin.site.register(Material, OrderedModelAdmin)
admin.site.register(Color, OrderedModelAdmin)
admin.site.register(Size, OrderedModelAdmin)
