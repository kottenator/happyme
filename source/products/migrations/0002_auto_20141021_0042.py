# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='product',
            name='manufacturer',
        ),
        migrations.AddField(
            model_name='collection',
            name='author',
            field=models.ForeignKey(blank=True, to='products.Author', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='collection',
            name='manufacturer',
            field=models.ForeignKey(blank=True, to='products.Manufacturer', null=True),
            preserve_default=True,
        ),
    ]
