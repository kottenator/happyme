# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import products.models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, related_name='children', null=True, to='products.Category')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
                ('description', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
                'ordering': ('order_number',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
                ('code', models.CharField(blank=True, null=True, max_length=100)),
                ('image', models.ImageField(blank=True, upload_to='colors', null=True)),
            ],
            options={
                'abstract': False,
                'ordering': ('order_number',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
                ('country', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
                'ordering': ('order_number',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
                'ordering': ('order_number',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
                ('id', models.CharField(serialize=False, primary_key=True, max_length=20, verbose_name='Article')),
                ('description', models.TextField(blank=True)),
                ('available_amount', models.IntegerField(default=0)),
                ('official_price_in_usd', models.FloatField(default=0.0)),
                ('purchase_price_in_usd', models.FloatField(default=0.0)),
                ('price_in_usd', models.FloatField(default=0.0)),
                ('price_in_uah', models.FloatField(blank=True, null=True, default=0.0)),
                ('sale_price_in_uah', models.FloatField(blank=True, null=True)),
                ('category', mptt.fields.TreeForeignKey(to='products.Category')),
                ('collection', models.ForeignKey(blank=True, null=True, to='products.Collection')),
                ('color', models.ForeignKey(blank=True, null=True, to='products.Color')),
                ('manufacturer', models.ForeignKey(blank=True, null=True, to='products.Manufacturer')),
                ('material', models.ForeignKey(blank=True, null=True, to='products.Material')),
            ],
            options={
                'abstract': False,
                'ordering': ('order_number',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('image', models.ImageField(upload_to=products.models.upload_product_image_to)),
                ('description', models.TextField(blank=True)),
                ('product', models.ForeignKey(related_name='images', to='products.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('order_number', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
                'ordering': ('order_number',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='product',
            name='size',
            field=models.ForeignKey(blank=True, null=True, to='products.Size'),
            preserve_default=True,
        ),
    ]
