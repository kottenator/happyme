import os
from compressor.filters import CompilerFilter
from compressor.filters.css_default import CssAbsoluteFilter
from django.contrib.staticfiles import finders
from conf.settings import BASE_DIR


__all__ = ['NodeSassAutoprefixer']


class ForcedCssAbsoluteFilter(CssAbsoluteFilter):
    def find(self, basename):
        return finders.find(basename)


class NodeSassAutoprefixer(CompilerFilter):
    def __init__(self, content, attrs, **kwargs):
        static_dirs = ['']

        apps_finder = finders.get_finder('django.contrib.staticfiles.finders.AppDirectoriesFinder')
        for storage in apps_finder.storages.values():
            static_dirs.append(storage.location)

        load_paths = ' --include-path '.join(static_dirs)
        command = 'node-sass %s {infile} {outfile} && autoprefixer {outfile}' % load_paths
        super(NodeSassAutoprefixer, self).__init__(content, command=command, **kwargs)

    def input(self, **kwargs):
        content = super(NodeSassAutoprefixer, self).input(**kwargs)
        return ForcedCssAbsoluteFilter(content).input(**kwargs)
