import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(BASE_DIR, 'source'))

SECRET_KEY = 'ndpe1xclf_hzv9%pin5474(&_7o4b2p0xlz3n+72@l385&t9*%'
ROOT_URLCONF = 'conf.urls'
WSGI_APPLICATION = 'conf.wsgi.application'
LANGUAGE_CODE = 'en'
TIME_ZONE = 'Europe/Kiev'
USE_TZ = True
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'var', 'static')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'var', 'media')
SITE_URL = 'http://happyme.com.ua/'
