import sys
from .base import *
from .apps import *

try:
    from .local import *
except ImportError:
    sys.stderr.write('\nYou should create your Django project local settings file: conf/settings/local.py '
                     '(you will find few samples in that directory)\n\n')
    sys.exit(1)
