import os
from .base import BASE_DIR

#--------------------------------------------------- Django settings ---------------------------------------------------
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

#---------------------------------------------------- Our own apps -----------------------------------------------------
INSTALLED_APPS += (
    'common',
    'products',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'products.context_processors.categories_menu',
    'products.context_processors.site_url',
)

#-------------------------------------------------- Third-party apps ---------------------------------------------------
# django-bower
INSTALLED_APPS += ('djangobower',)
STATICFILES_FINDERS += ('djangobower.finders.BowerFinder',)
BOWER_COMPONENTS_ROOT = os.path.join(BASE_DIR, 'var', 'lib')
BOWER_INSTALLED_APPS = (
    'jquery#2.1.1',
    'underscore#1.6.0',
    'html5shiv#3.7.2',
    'respond#1.4.2',
    'imagesloaded#3.1.8',
)

# django-sekizai
INSTALLED_APPS += ('sekizai',)
TEMPLATE_CONTEXT_PROCESSORS += ('sekizai.context_processors.sekizai',)

# django-compressor
INSTALLED_APPS += ('compressor',)
STATICFILES_FINDERS += ('compressor.finders.CompressorFinder',)
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'conf.compressor.precompilers.NodeSassAutoprefixer'),
)

# django-mptt
INSTALLED_APPS += ('mptt',)

# sorl-thumbnail
INSTALLED_APPS += ('sorl.thumbnail',)

# Gunicorn
INSTALLED_APPS += ('gunicorn',)

# django-supervisor
INSTALLED_APPS += ('djsupervisor',)
SUPERVISOR_CONFIG_FILE = os.path.join(BASE_DIR, 'conf/supervisor/supervisor.conf')
