Happy Me
========
Homemade webshop for homemade tailors.

This is just a simple webshop to offer tailor goods like fabric, accessories, etc.

I'm developing it as a gift for my wife :) It should be available on http://happyme.com.ua/

Prepare to install
------------------
First you need to satisfy pre-requisites:

  - you need Python (tested on 2.7 and 3.4)
  - install pip & virtualenv
  - [prepare yourself](http://pillow.readthedocs.org/en/latest/installation.html) to install Pillow
  - [install SCSS](http://sass-lang.com/install)
  - [install Autoprefixer](https://github.com/ai/autoprefixer#others)

Installation (Linux)
--------------------
```bash
git clone git@bitbucket.org:kottenator/happyme.git
cd happyme 
export PROJECT_ROOT="`pwd`"

# Make virtualenv
cd $PROJECT_ROOT/var/virtualenv
virtualenv happyme
. happyme/bin/activate

# Install requirements
cd $PROJECT_ROOT
pip install -r requirements.pip

# Configure local Django settings
cd $PROJECT_ROOT/conf/settings
cp local.py.dev-sample local.py
nano local.py

# Init DB
cd $PROJECT_ROOT
./manage.py syncdb --migrate

# Install JS/CSS libs
./manage.py bower_install
```

For local environment:
```bash
# Run dev server
./manage.py runserver
```

For production environment:
```bash
# Configure local Gunicorn settings
cd $PROJECT_ROOT/conf/gunicorn
cp gunicorn.conf.sample gunicorn.conf # you may edit it

# Configure local Supervisor settings
cd $PROJECT_ROOT/conf/supervisor
cp supervisor.conf.sample supervisor.conf # you may edit it

cd $PROJECT_ROOT

# Update static files
./manage.py collectstatic --no-input
./manage.py compress --force

# Run prod server
./manage.py supervisor --daemon
```